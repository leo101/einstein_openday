import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class About extends StatelessWidget {
  final double version;
  About(this.version);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //Barra superiore
      appBar: AppBar(
        title: Text(
          'Informazioni',
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        elevation: 0.0,
      ),
      body: Center(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              //Immagine di me medesimo
              Tooltip(
                message: 'Scarica BalzApp premendo qui',
                child: SizedBox.fromSize(
                  size: new Size(220.0, 220.0),
                  child: IconButton(
                    icon: ClipOval(
                      child: Image.asset(
                        "lib/images/Foto.jpg",
                        fit: BoxFit.cover,
                        width: 220.0,
                        height: 220.0,
                      ),
                    ),
                    //Alla pressione apre la pagina di BalzApp sul playStore
                    onPressed: () async {
                      const url =
                          'https://play.google.com/store/apps/details?id=com.leonardopizio.balzapp';

                      if (await canLaunch(url)) {
                        await launch(
                          url,
                          forceWebView: false,
                          enableJavaScript: true,
                        );
                      }
                    },
                  ),
                ),
              ),
              //In fondo alla pagina viene mostrato il logo e le info
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      "lib/images/iisLogo.png",
                      height: 150.0,
                      width: 150.0,
                    ),
                    Text(
                      'Applicazione creata da Leonardo Pizio\nOpenDay IIS Einstein Vimercate, V${version.toString()} ${version < 1.0 ? 'Beta' : ''}',
                      style: TextStyle(fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      //Fab per chiudere la pagina delle informazioni
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Colors.white,
        label: Text(
          'Indietro',
          style: TextStyle(color: Colors.black),
        ),
        icon: Icon(
          Icons.arrow_back,
          color: Colors.blueAccent,
          size: 35.0,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}
