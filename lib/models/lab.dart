import 'package:flutter/material.dart';

class Laboratorio {
  String nome;
  final String descrizione;
  final Color colore;
  bool main;
  List<AssetImage> images = [];
  List<Laboratorio> interni = [];

  Laboratorio(this.nome,
      {this.descrizione, this.images, this.interni, this.colore, this.main});
  
  List<AssetImage> getImages() {
    return images == null ? [] : images;
  }
}
