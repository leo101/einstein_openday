import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import './home.dart';
import './about.dart';

import 'models/lab.dart';

void main() => runApp(new MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  double version = 1.0; //Versione dell'App
  //Lista degli indirizzi
  List<Laboratorio> labs = [];
  //Lista di tutti i laboratori
  List<Laboratorio> inLab = [
    new Laboratorio('Alla scoperta dei mestieri',
        descrizione:
            'Luciano Perrone insieme al prof. Mascheroni, fotoreporter e giornalista, coordinatore del gruppo fotografico presenta i lavori prodotti dal gruppo.'),
    new Laboratorio('Liceo S.A. Sportivo',
        descrizione:
            'La prof. Riva insieme al prof. Lometti presentano le attività, il percorso del Liceo Scientifico SA con potenziamento Sportivo'),
    new Laboratorio('Liceo S.A. Linguistico',
        descrizione:
            'La prof. Silva Marina presenta il Liceo Scientifico con opzione Scienze Applicate, il quadro orario, le attività, le metodologie. Vengono, inoltre, presentate le possibilità potenziamento Linguistico'),
    new Laboratorio('Biotecnologie',
        descrizione:
            'La prof. Erba Luisa presenta l’indirizzo Biotecnologie ambientali e sanitario: il quadro orario, le discipline di indirizzo, le attività laboratoriali, gli sbocchi occupazionali.'),
    new Laboratorio('Elettronica'),
    new Laboratorio('Dall\'idea al progetto',
        descrizione:
            'Il prof. Nava insieme agli studenti presentano i progetti svolti.'),
    new Laboratorio('Il Robot....il tuo futuro',
        descrizione:
            'Il prof. Cerri presenta l’indirizzo Elettronico, con le sue articolazioni: elettronica ed Automazione; i progetti sviluppati.'),
    new Laboratorio('Il mondo nella ...Rete',
        descrizione:
            'Il prof. Iannì presenta l’indirizzo Informatico, con particolare riferimento al mondo delle reti informatiche, il percorso CISCO e le certificazioni.'),
    new Laboratorio('Un viaggio nel mondo dell\'Informatica e del web',
        descrizione:
            'La prof. Mariani presenta l’indirizzo informatico con riferimento alle discipline di informatica e progettazione. Le attività, i progetti, le prospettive future.'),
    new Laboratorio('Il mondo dell\'IOT',
        descrizione:
            'Il prof. Catalano presenta le applicazioni del mondo IOT attraverso gli apparati di rete CISCO.'),
    new Laboratorio('Il mondo della Fisica!',
        descrizione:
            'Il prof. Micene insieme ai suoi alunni presenterà una serie di interessanti attività svolte in laboratorio di fisica.'),
    new Laboratorio('I misteri della materia',
        descrizione:
            'Il prof. Genovese con i suoi alunni alla scoperta della materia.'),
    new Laboratorio('Gli aromi della natura',
        descrizione:
            'Il prof. D’ambrosio presenta come la chimica è presenta intorno a noi.'),
    new Laboratorio('Diamo un nome alle sostanze',
        descrizione:
            'La prof. Santi e le formule chimiche; un affascinante percorso nel mondo delle formule che ci governano'),
    new Laboratorio('Il DNA e la vita',
        descrizione:
            'La prof. Borrelli svelerà i misteri del DNA in un interessante attività di laboratorio'),
    new Laboratorio('Dall\'idea al prodotto con l\'argilla',
        descrizione:
            'La prof. Del Zoppo insieme ai suoi alunni presenta come è possibile manipolare e modellizzare l’argilla. Un laboratorio artistico che coinvolge gli studenti in un mondo fatto di idee e creatività.'),
    new Laboratorio('L\'aula del pittore',
        descrizione:
            'La prof. Anzaghi insieme ai suoi alunni nell’affascinante mondo della pittura!'),
    new Laboratorio('Le discipline geometriche',
        descrizione:
            'Gli alunni del nuovo Liceo Artistico alle prese con il disegno geometrico in un mondo di interessanti forme e colori.'),
    new Laboratorio('Progettiamo insieme',
        descrizione:
            'Il prof. Cocina insieme ai suoi alunni al lavoro con la progettazione mediante Autocad'),
    new Laboratorio('Dall\'idea all\'oggetto',
        descrizione:
            'Il prof. Previtali dimostra come avviene la progettazione di un “oggetto” a partire dall’ideazione fino alla stampa in 3D'),
    new Laboratorio('Animiamoci',
        descrizione:
            'Andrea Ligabo’, esperto in fumettistica e animazione, presenta le possibilità che offre il mondo dell’audiovisivo-multimediale con riferimento al mondo delle animazioni'),
  ];

  @override
  void initState() {
    _makeLabs();
    super.initState();
  }

  //Funzione per l'inserimento degli indirizzi
  _makeLabs() {
    inLab.forEach((e) {
      e.main = false;
    });
    labs.add(new Laboratorio(
      'Elettronica',
      images: [
        AssetImage('lib/images/laboratori/Elettronica.JPG'),
      ],
      main: true,
      interni: [
        inLab[5],
        inLab[6],
        inLab[0],
        inLab[9],
        inLab[10],
        inLab[19],
      ],
      colore: Colors.blueGrey[700],
    ));
    labs.add(new Laboratorio(
      'Informatico',
      images: [
        AssetImage('lib/images/laboratori/Informatica.JPG'),
      ],
      interni: [
        inLab[9],
        inLab[7],
        inLab[8],
        inLab[0],
        inLab[10],
        inLab[19],
      ],
      main: true,
      colore: Colors.blueGrey[700],
    ));
    labs.add(new Laboratorio(
      'Liceo Scientifico',
      images: [
        AssetImage('lib/images/laboratori/Liceo_Scientifico.JPG'),
      ],
      interni: [
        inLab[1],
        inLab[10],
        inLab[11],
        inLab[0],
        inLab[14],
        inLab[18],
        inLab[19],
      ],
      main: true,
      colore: Colors.blueGrey[700],
    ));
    labs.add(new Laboratorio(
      'Liceo Artistico',
      images: [
        AssetImage('lib/images/laboratori/Artistico.JPG'),
      ],
      interni: [
        inLab[0],
        inLab[15],
        inLab[16],
        inLab[17],
        inLab[18],
        inLab[20],
        inLab[19],
      ],
      main: true,
      colore: Colors.blueGrey[700],
    ));
    labs.add(new Laboratorio(
      'Biotecnologie Sanitario',
      images: [
        AssetImage('lib/images/laboratori/Biotecnologie.JPG'),
      ],
      interni: [
        inLab[14],
        inLab[12],
        inLab[13],
        inLab[0],
        inLab[10],
        inLab[11],
        inLab[19],
      ],
      main: true,
      colore: Colors.blueGrey[700],
    ));
    labs.add(
      new Laboratorio(
        'Einstein',
        colore: Colors.blueGrey[700],
      ),
    );
    //Aggiungo la foto del logo dell'einstein e la imposto come prima
    labs.forEach((e) {
      e.images != null
          ? e.images.add(
              AssetImage('lib/images/iisLogo.png'),
            )
          : e.images = [
              AssetImage('lib/images/iisLogo.png'),
            ];
      if (e.images.length >= 2) {
        AssetImage image;
        image = e.images[0];
        e.images[0] = e.images[1];
        e.images[1] = image;
      }
    });
    inLab.forEach((e) {
      e.images = [
        AssetImage('lib/images/iisLogo.png'),
      ];
      if (e.images.length >= 2) {
        AssetImage image;
        image = e.images[0];
        e.images[0] = e.images[1];
        e.images[1] = image;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    //Metto l'app in FullScreen e blocco la possibilita' di rotazione
    SystemChrome.setEnabledSystemUIOverlays([]);
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    return MaterialApp(
      title: 'IIS Einstein OpenDay',
      routes: {
        '/': (context) => Home(labs),
        '/info': (context) => About(version),
      },
    );
  }
}
