import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:url_launcher/url_launcher.dart';

import 'models/lab.dart';

import 'labPage.dart';

class Home extends StatelessWidget {
  final List<Laboratorio> labs; //lista degli indirizzi

  //Apre il sito dell'einstein
  _sendEinstein() async {
    const url = 'https://www.einsteinvimercate.gov.it/';

    if (await canLaunch(url)) {
      await launch(
        url,
        forceWebView: false,
        enableJavaScript: true,
      );
    }
  }

  Home(this.labs);
  @override
  Widget build(BuildContext context) {
    const double contRadius = 15.0;
    return Scaffold(
      //Barra superiore
      appBar: new AppBar(
        elevation: 0.0,
        title: Row(
          children: <Widget>[
            InkWell(
              child: Image.asset('lib/images/ShortLogo.png'),
              onTap: () => _sendEinstein(),
            ),
            Text(
              'IIS Einstein',
              style: TextStyle(color: Colors.white),
            ),
          ],
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.blueGrey[600],
      ),
      //Creazione dei rettangoli per gli indirizzi
      body: ListView(
        children: <Widget>[
          GridView.count(
            shrinkWrap: true,
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
            crossAxisSpacing: 4.0,
            mainAxisSpacing: 4.0,
            crossAxisCount: 2,
            childAspectRatio: 1.5,
            children: List.generate(labs.length, (index) {
              return InkWell(
                onTap: () {
                  //Se viene premuto il rettangolo dell'Einstein, apre il sito, altrimenti porta alla pagina dell'indirizzo
                  labs[index].nome != 'Einstein'
                      ? Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  LabInfo(labs[index])),
                        )
                      : _sendEinstein();
                },
                //Piccolo Easter Egg
                onLongPress: () {
                  if (labs[index].nome == 'Liceo Artistico') {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return SimpleDialog(
                            title: Text('MadMan'),
                            children: <Widget>[
                              Center(
                                  child: Text(
                                      'Ho fatto lo scientifico, mica l\'artistico')),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  FlatButton(
                                    child: Text('Ok'),
                                    textColor: Colors.blue[300],
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                  ),
                                ],
                              ),
                            ],
                          );
                        });
                  }
                },
                child: labs[index].nome != 'Einstein'
                    ? Center(
                        child: Container(
                          decoration: new BoxDecoration(
                              color: labs[index].colore,
                              borderRadius: new BorderRadius.only(
                                  topLeft: const Radius.circular(contRadius),
                                  bottomLeft: const Radius.circular(contRadius),
                                  bottomRight:
                                      const Radius.circular(contRadius),
                                  topRight: const Radius.circular(contRadius))),
                          child: Center(
                            child: AutoSizeText(
                              '${labs[index].nome}',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white, fontSize: 20.0),
                              maxLines: 3,
                            ),
                          ),
                        ),
                      )
                    : InkWell(
                        onTap: () async {
                          const url =
                              'https://www.einsteinvimercate.gov.it/webquest/index.php/955755?lang=it';

                          if (await canLaunch(url)) {
                            await launch(
                              url,
                              forceWebView: false,
                              enableJavaScript: true,
                            );
                          }
                        },
                        child: Image.asset('lib/images/iscriviti.png'),
                      ),
              );
            }),
          ),
          Container(
            padding: EdgeInsets.all(10.0),
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 200,
              child: InkWell(
                onTap: () async {
                  const url = 'https://youtu.be/aBuJyocX38E';

                  if (await canLaunch(url)) {
                    await launch(
                      url,
                      forceWebView: false,
                      enableJavaScript: true,
                    );
                  }
                },
                child: Image.asset('lib/images/Presentazione.png'),
              ),
            ),
          ),
        ],
      ),
      //FAB che porta alle pagina delle informazioni
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.white,
        onPressed: () {
          Navigator.pushNamed(context, '/info');
        },
        child: Stack(
          children: <Widget>[
            Icon(
              Icons.fiber_manual_record,
              color: Colors.blue,
              size: 55.0,
            ),
            Icon(
              Icons.info,
              size: 55.0,
            ),
          ],
        ),
      ),
    );
  }
}
