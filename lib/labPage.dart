import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:photo_view/photo_view.dart';

import 'models/lab.dart';

class LabInfo extends StatelessWidget {
  final Laboratorio lab;
  LabInfo(this.lab);

  //Funzione per la creazione dei widget contenenti le immagini
  Widget _itemBuilder(BuildContext context, int index) {
    return Row(
      children: <Widget>[
        InkWell(
          child: Hero(
            tag: 'HeroIMG$index',
            child: Image(
              image: lab.getImages()[index],
              key: new Key('Image$index'),
              fit: BoxFit.cover,
            ),
          ),
          //Alla pressione viene aperta la pagina con l'immagine ingrandita
          onTap: () {
            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (BuildContext context) =>
                        ImagePage(lab.images[index], index)));
          },
        ),
        //Per spaziare le immagini
        SizedBox(
          width: 10.0,
        ),
      ],
    );
  }

  Widget _singleItemBuilder(BuildContext context, int index) {
    return Container();
  }

  Widget _inLabBuilder(BuildContext context, int index) {
    return InkWell(
        child: Card(
          color: Colors.red[800],
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                width: 15.0,
              ),
              Text(
                '${lab.interni[index].nome}',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 15.0),
              ),
              SizedBox(
                height: 35.0,
              ),
            ],
          ),
        ),
        //Alla pressione si apre una pagina con piu' informazioni
        onTap: () {
          Navigator.push(
            context,
            new MaterialPageRoute(
              builder: (BuildContext context) => LabInfo(lab.interni[index]),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    const double contRadius = 15.0;
    return Scaffold(
      body: InkWell(
        onTap: () {
          Navigator.pop(context);
        },
        child: Container(
          color: Colors.transparent,
          padding: EdgeInsets.all(10.0),
          child: Container(
            padding: EdgeInsets.all(10.0),
            decoration: new BoxDecoration(
                color: Colors.blueGrey[600],
                borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(contRadius),
                    bottomLeft: const Radius.circular(contRadius),
                    bottomRight: const Radius.circular(contRadius),
                    topRight: const Radius.circular(contRadius))),
            child: ListView(
              children: <Widget>[
                //Se esistono immagini, vengono mostrate
                lab.getImages() != []
                    ? SizedBox(
                        height: 150.0,
                        width: 500.0,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: lab.getImages().length == 0
                              ? 1
                              : lab.getImages().length,
                          itemBuilder: lab.getImages().length == 0
                              ? _singleItemBuilder
                              : _itemBuilder,
                        ),
                      )
                    : Container(),
                SizedBox(
                  height: 20.0,
                ),
                //Se e' un indirizzo, viene mostrata la scritta
                lab.main
                    ? Text(
                        'Indirizzo:',
                        style: TextStyle(fontSize: 13.0, color: Colors.white),
                      )
                    : Container(),
                AutoSizeText(
                  '${lab.nome}',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 25.0),
                  maxLines: 3,
                ),
                //Se e' presente una descrizione, viene visualizzata
                lab.descrizione != null
                    ? AutoSizeText(
                        '${lab.descrizione}',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20.0),
                      )
                    : Container(),
                SizedBox(
                  height: 50.0,
                ),
                //Se esistono laboratori, viene visualizzata la scritta Laboratori:
                lab.interni != null
                    ? Center(
                        child: Text(
                          'Laboratori:',
                          style: TextStyle(color: Colors.white, fontSize: 15.0),
                        ),
                      )
                    : Container(),
                //Se esistono laboratori, vengono visualizzati
                lab.interni != null
                    ? SizedBox(
                        height: (lab.interni.length * 80.0) / 2 + 20.0,
                        child: Theme(
                          data: ThemeData(
                            accentColor: Colors.blueGrey,
                          ),
                          child: ListView.builder(
                            itemCount:
                                lab.interni != null ? lab.interni.length : 0,
                            itemBuilder: _inLabBuilder,
                          ),
                        ),
                      )
                    : Container(),
                //Se la descrizione non e' nulla ==> va visualizzata la cartina del lab.
                lab.descrizione != null
                    ? SizedBox(
                        height: 200.0,
                        width: 250.0,
                        child: InkWell(
                          child: Hero(
                              tag: 'HeroIMG-1',
                              child: Image.asset(
                                  'lib/images/Piantine/Piantina Modificata${lab.nome}.png')),
                          onTap: () {
                            Navigator.push(
                                context,
                                new MaterialPageRoute(
                                    builder: (BuildContext context) => ImagePage(
                                        AssetImage(
                                            'lib/images/Piantine/Piantina Modificata${lab.nome}.png'),
                                        -1)));
                          },
                        ),
                      )
                    : Container(),
                lab.descrizione != null
                    ? Center(
                        child: Text(
                          'Clicca sull\'immagine per ingrandirla.',
                          style: TextStyle(color: Colors.white, fontSize: 15.0),
                        ),
                      )
                    : Container(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ImagePage extends StatelessWidget {
  final AssetImage immagine;
  final int index;
  ImagePage(this.immagine, this.index);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: InkWell(
        onTap: () {
          Navigator.pop(context);
        },
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: PhotoView(
                  heroTag: 'HeroIMG$index',
                  imageProvider: immagine,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
